# Segunda actividad

En este directorio, los estudiantes deberán crear un archivo de texto plano
respetando la siguientes características:

## Nombre del archivo

* El nombre debe estar en [snake case](http://en.wikipedia.org/wiki/Snake_case)
* No debe tener extensión
* El nombre será el nombre del grupo de GitLab

## Contenido del archivo

* Archivo separado por *coma (,)* [Archivo CSV](https://es.wikipedia.org/wiki/Valores_separados_por_comas)
* Campos en el archivo:
  * Nombre de usuario
  * Rol asignado en gitlab

## ¿Como proceder?
* Crear el nombre del grupo (Recomendacion: visibilidad publico)
* Hacer hacer la bifurcacion de esta actividad en el repositorio
* Crear el archivo
* Pedir la solicitud de cambio *merge request* (De esta manera el resto de los estudiantes podra ver los grupos)
